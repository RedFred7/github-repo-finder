# Github Language Finder

Utility application to find Github users' favourite languages

## Description


GLF is a Rails app that allows the user to input a Github username and presents the languages the user's repositories are based on, in order of popularity.

## Usage


Clone this repo: 

	git clone https://gitlab.com/RedFred7/github-repo-finder.git

Then cd into the application directory and install gems:

	bundle install

Now run the server:

	rails -s -p [port number]
	
You can now go to your browser and access	the app at http://localhost:<port number]

## Development notes


This application has been developed and tested with Ruby 2.3.3 and Rails 5.0.1. It should work with any 2.x Ruby version and Rails 5.x version, though there is no guarantee.

### Testing

You can run all tests for this app by running

	rake

within the application directory