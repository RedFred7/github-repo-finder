Rails.application.routes.draw do
  root to: 'github_users#new'
  resources :github_users, only: [:create, :new, :show]
end
