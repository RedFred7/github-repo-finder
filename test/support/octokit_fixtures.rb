module OctokitFixtures
  # Return the path to the Octokit fixtures directory
  def octo_fix_dir
    File.join Rails.root, "test/fixtures/octokit"
  end

  # Return a filename for a fixture
  def octo_fix_file(filename)
    File.join octo_fix_dir, filename
  end

  # Return the contents of a fixture as a Ruby data structure
  def octo_fix_struct(filename)
    eval(File.open(octo_fix_file(filename)).read)
  end

  # Return the contents of a fixture as a single resource
  def octo_fix_resource_single(filename)
    agent = Sawyer::Agent.new('http://localhost:3000')
    Sawyer::Resource.new(agent, octo_fix_struct(filename))
  end

  # Return the contents of a fixture as Array of resources
  def octo_fix_resource_many(filename)
    res = Array.new
    agent = Sawyer::Agent.new('http://localhost:3000')
    octo_fix_struct(filename).each do |e|
      res << Sawyer::Resource.new(agent, e)
    end
    res
  end
end
