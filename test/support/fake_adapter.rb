require_relative 'octokit_fixtures'

class FakeAdapter
  include OctokitFixtures
  attr_reader :errors, :user, :repo_list

  def initialize(valid_user=true)
    @errors = Array.new
    @user = octo_fix_resource_single('user.rb') if valid_user
    @repo_list = octo_fix_resource_many('repos.rb') if valid_user
  end

  def language_listings
    {Ruby: 9, HTML: 1}
  end

end
