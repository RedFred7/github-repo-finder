ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
Dir[Rails.root.join("test/support/**/*.rb")].each { |f| require f }
Dir[Rails.root.join("test/helpers/**/*.rb")].each { |f| require f }
require 'minitest/pride'
require 'mocha/mini_test'
require 'webmock/minitest'
WebMock.disable_net_connect!


class ActiveSupport::TestCase
  # Add more helper methods to be used by all tests here...
end

Minitest::Reporters.use!(
  Minitest::Reporters::SpecReporter.new,
  ENV,
  Minitest.backtrace_filter
)