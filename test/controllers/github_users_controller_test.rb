require 'test_helper'

class GithubUsersControllerTest < ActionDispatch::IntegrationTest

  setup do
    Adapter::GitHub.stubs(:new).with(any_parameters).returns(FakeAdapter.new(true))
  end

  test "new github user" do
    get new_github_user_url
    assert_response :ok
  end

  test "show github user" do
    langs = {Ruby: 9, HTML: 1}
    get github_user_url 'RedFred7'
    assert_response :ok
  end

  test "show github non-existent user" do
  	Adapter::GitHub.stubs(:new).with(any_parameters).returns(FakeAdapter.new(false))
    get github_user_url 'xxxxzzzzz'
    assert_response :missing
  end

  test "create with valid params creates and redirects to user page" do
    data = { username: 'RedFred7' }
    post github_users_url, params: data
    assert_response :found
    assert_redirected_to github_user_url(data[:username])
  end

  test "create with no params creates error" do
    data = { }
    post github_users_url, params: data
    assert_response :bad_request
  end
end
