require 'test_helper'

class GithubUsersControllerTest < ActionDispatch::IntegrationTest

  setup do
    Adapter::GitHub.stubs(:new).with(any_parameters).returns(FakeAdapter.new(true))
  end

  test "new github user - JSON" do
    get new_github_user_url(format: :json)
    assert_equal Hash.new, JSON.parse(response.body)
  end

  test "show github user - JSON" do
    get github_user_url(format: :json, id:'RedFred7')
    res = JSON.parse(response.body)
    assert_equal 9, res['Ruby']
  end

  test "show github non-existent user - JSON" do
    Adapter::GitHub.stubs(:new).with(any_parameters).returns(FakeAdapter.new(false))
    get github_user_url(format: :json, id: 'xxxxzzzzz')
    assert_equal Hash.new, JSON.parse(response.body)
  end

  test "create with valid params creates user - JSON" do
    usrname = 'RedFred7'
    msg = "User #{usrname} was found."
    data = { username: usrname }
    post github_users_url(format: :json), params: data
    res = JSON.parse(response.body)
    assert_equal msg, res['message']
  end

  test "create with no params creates error - JSON" do
    data = { }
    msg = 'No username provided'
    post github_users_url(format: :json), params: data
    res = JSON.parse(response.body)
    assert_equal msg, res['message']
  end
end
