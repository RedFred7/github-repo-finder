require_relative '../test_helper'

class AdapterTest < Minitest::Test
  include OctokitFixtures

  def setup
    user_data = octo_fix_struct('user.rb')
    @adapter = Adapter::GitHub.new('RedFred7')
    @adapter.stubs(:user).returns(user_data)
  end

  def test_repo_list
    result = octo_fix_struct('repos.rb')
    @adapter.expects(:repo_list).returns(result)
    assert_equal result, @adapter.repo_list
  end

  def test_language_listings
    repo_data = octo_fix_resource_many('repos.rb')
    @adapter.stubs(:repo_list).returns(repo_data)
    assert_equal 9,  @adapter.language_listings[:Ruby]
    assert_equal 1,  @adapter.language_listings[:HTML]
  end
end
