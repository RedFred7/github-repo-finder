class GithubUsersController < ApplicationController
  protect_from_forgery except: :create

  # GET /github_users/:id
  # GET /github_users/:id.json
  def show
    respond_to do |format|
      setup_github_client
      if @github.user
        format.html { render :show, status: :ok }
        format.json { render json: @github.language_listings, status: :ok }
      else
        format.html { render :show, status: :not_found }
        format.json { render json: {}, status: :not_found }
      end
    end
  end

  # GET /github_users/new
  # GET /github_users/new.json
  def new
    respond_to do |format|
      format.html # new.html.erb
      format.json  { render json: {}, status: :ok }
    end
  end


  # POST /github_users
  # POST /github_users.json
  def create
    respond_to do |format|
      if acceptable_username?
        setup_github_client
        if @github.user
          respond_to_success(format, "User #{params[:username]} was found.", github_user_url(@github.user.login))
        else
          respond_to_error(format, @github.errors.first.message, @github.errors.first.response_status)
        end
      else
        respond_to_error(format, "No username provided", :bad_request, false)
      end
    end
  end

  private
  def respond_to_success(format, msg, new_path)
    format.html { redirect_to(new_path) }
    format.json { render json: {message: msg}, status: :created }
  end

  def respond_to_error(format, msg, status, show_flash=true)
    flash.now[:alert] = "Error: #{msg}" if show_flash
    format.html { render :new, status: status }
    format.json { render json: {message: msg}, status: status }
  end

  def acceptable_username?
    params[:username] && !params[:username].empty?
  end

  def user_params
    params.permit(:id, :username)
  end

  def setup_github_client(client=Adapter::GitHub, args=user_params)
    @github ||= client.new(args)
  end
end
