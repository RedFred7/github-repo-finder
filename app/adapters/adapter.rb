module Adapter
  class GitHub
    attr_reader :errors, :user, :repo_list

    def initialize(params)
      @errors = Array.new
      username = params[:username] || params[:id]
      @user ||= Octokit.user username
      @repo_list ||= @user.rels[:repos].get.data
    rescue => error
      @errors << error
    end

    # counts repo_list and sums up the number each language appears
    # in the repos
    #
    # @return Hash key = language, value = number of occurrences
    def language_listings
      langs = Hash.new(0)
      return langs unless repo_list && !repo_list.empty?
      repo_list.each do |repo|
        langs[repo.language.to_sym] += 1 if repo.language
      end
      langs
    end

  end
end
