module GithubUsersHelper
  def order_list(a_hash)
    Hash[a_hash.sort_by{|k, v| v}.reverse]
  end
end
